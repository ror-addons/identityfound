<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
	<UiMod name="IdentityFound" version="1.2" date="2008-12-20" >
	<Author name="Incrediclint" email="none" />
	<Description text="Names display options" />
		<Dependencies>
			<Dependency name="LibSlash" />
		</Dependencies>		
		<Files>
			<File name="IdentityFound.lua" />
		</Files>
		<SavedVariables>
            <SavedVariable name="IdentityFound.Settings" />
		</SavedVariables>
		<OnInitialize>
            	<CallFunction name="IdentityFound.Initialize" />
		</OnInitialize>
        <OnUpdate /> 
		<OnShutdown />	
	</UiMod>
</ModuleFile>