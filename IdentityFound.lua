IdentityFound = {}

if not IdentityFound.Settings then

	IdentityFound.Settings = {
		AllOff = 
		{
			yourhealth = 1,
			friendlyplayers = false,
			enemynpcs = false,
			enemytitles = false,
			yourtitle = false,
			enemyplayers = false,
			target = false,
			your = false,
			friendlynpcs = false,
			enemyguilds = false,
			friendlyguilds = false,
			npchealth = 1,
			friendlytitles = false,
			yourpet = false,
			npctitles = false,
			targethealth = 1,
			targetguild = false,
			targettitle = false,
			grouphealth = 1,
			friendlyhealth = 1,
			enemyhealth = 1,
		},
		Guild =
		{
			yourhealth = 1,
			friendlyplayers = false,
			enemynpcs = false,
			enemytitles = false,
			yourtitle = false,
			enemyplayers = false,
			target = false,
			your = false,
			friendlynpcs = false,
			enemyguilds = false,
			targethealth = 3,
			friendlyguilds = true,
			npchealth = 2,
			enemyhealth = 2,
			friendlyhealth = 2,
			grouphealth = 2,
			targettitle = false,
			targetguild = false,
			npctitles = false,
			yourpet = false,
			friendlytitles = false,
		},
		RVR =
		{
			yourhealth = 1,
			friendlyplayers = false,
			enemynpcs = false,
			enemytitles = false,
			yourtitle = false,
			enemyplayers = false,
			target = true,
			your = false,
			friendlynpcs = false,
			enemyguilds = false,
			targethealth = 1,
			friendlyguilds = false,
			npchealth = 1,
			enemyhealth = 3,
			friendlyhealth = 2,
			grouphealth = 3,
			targettitle = false,
			targetguild = false,
			npctitles = false,
			yourpet = false,
			friendlytitles = false,
		},
		Default =
		{
			yourhealth = 1,
			friendlyplayers = true,
			enemynpcs = true,
			enemytitles = false,
			yourtitle = true,
			enemyplayers = true,
			target = false,
			your = true,
			friendlynpcs = true,
			enemyguilds = true,
			targethealth = 1,
			friendlyguilds = true,
			npchealth = 3,
			enemyhealth = 3,
			friendlyhealth = 3,
			grouphealth = 3,
			targettitle = true,
			targetguild = true,
			npctitles = true,
			yourpet = true,
			friendlytitles = true,
		},
	}
end

function IdentityFound.LoadSettings( src, tmp )
	for setting, value in pairs ( src ) do
		tmp[setting] = value
	end
end

function IdentityFound.Initialize()

	LibSlash.RegisterSlashCmd("idf", function(input) IdentityFound.handler(input) end)

end

function IdentityFound.handler(cmd)
	local opt = cmd:match("(%w+)[ ]?(.*)")
	if ( opt == "rvr" ) then
		-- IdentityFound.RVR()
		IdentityFound.LoadSettings( IdentityFound.Settings.RVR, SystemData.Settings.Names )
	elseif ( opt == "alloff" ) then
		IdentityFound.LoadSettings( IdentityFound.Settings.AllOff, SystemData.Settings.Names )
		--IdentityFound.AllOff()
	elseif ( opt == "default" ) then
		IdentityFound.LoadSettings( IdentityFound.Settings.Default, SystemData.Settings.Names )
		--IdentityFound.Guild()
	elseif ( opt == "guild" ) then
		IdentityFound.LoadSettings( IdentityFound.Settings.Guild, SystemData.Settings.Names )
		--IdentityFound.Default()
	elseif ( opt == "save" ) then
		IdentityFound.savesettings()
	end	
	BroadcastEvent( SystemData.Events.USER_SETTINGS_CHANGED )
end




